const Express     = require('express');
const app         = Express();
const router = Express.Router();
const Route = require("./route");
const BodyParser  = require('body-parser');

const port = process.env.PORT || 8000;


app.use(function (req, res, next) {
    res.setHeader("Access-Control-Allow-Origin", "*");
    res.setHeader("Access-Control-Allow-Methods", "POST, GET, PUT, OPTIONS, DELETE");
    res.setHeader("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization");
    res.setHeader('Access-Control-Allow-Credentials', true);
    next();
});


app.use(BodyParser.json({limit: '50mb'}));
app.use(BodyParser.urlencoded({extended : true}));

app.use('/commandes',Route);
app.listen(port, () => console.log(`Server running on port ${port}`));

